# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# imports
import sys
import matplotlib.pyplot as plt
import numpy as np
import math
from matplotlib.legend_handler import HandlerLine2D
from problem import Problem
from Abc     import Abc
from abcd    import Abcd

# Global configurations
pbInd = Problem.RASTRIGIN # Problem to be optimized
nDim  = 30                # Number of dimensions
nIter = 2000              # Number of iterations
nRuns = 50                # How many times to run it

# The main variables can be added on command line. Very useful for automatization
if len(sys.argv) > 1:
	pbInd = int(sys.argv[1])
if len(sys.argv) > 2:
	nDim = int(sys.argv[2])
if len(sys.argv) > 3:
	nIter = int(sys.argv[3])
if len(sys.argv) > 4:
	nRuns = int(sys.argv[4])

# ABC's configuration
NP = 30
limit = 50 #(NP/2)*pb1.D

# Just prints the configuration
print('pbInd: ' + str(Problem.names[pbInd]) + ', nDim: ' + str(nDim) + ', nIter: ' + str(nIter) + ' nRuns: ' + str(nRuns))

showDiversity = True

# Both algorithms execute same problem
pb1 = Problem(pbInd, nDim)
pb2 = Problem(pbInd, nDim)

# Convergence curves
evol0 = [0]*nIter
evol1 = [0]*nIter
# Diversity over all optimization
diversity1 = [0]*nIter
diversity2 = [0]*nIter

# Best found
final_abc = [0]*nRuns
final_abcd = [0]*nRuns


for run in range(nRuns):
	abc = Abc(pb1,NP,limit,nIter)
	abcd = Abcd(pb2,NP,limit,nIter)

	abc.initall()
	abcd.initall()

	iteration = 0
	while iteration < nIter:
		abc.iterate()
		abcd.iterate()

		evol0[iteration] = evol0[iteration] + abc.globalMin/nRuns
		evol1[iteration] = evol1[iteration] + abcd.globalMin/nRuns

		diversity1[iteration] = diversity1[iteration] + abc.popDiversity()/nRuns
		diversity2[iteration] = diversity2[iteration] + abcd.popDiversity()/nRuns

		iteration = iteration + 1
	
	final_abc[run]  = abc.globalMin
	final_abcd[run] = abcd.globalMin

# Mean of the best results found for both versions
mean0 = sum(final_abc)/nRuns
mean1 = sum(final_abcd)/nRuns

print(pb1.name)
print('ABC: ' + str(mean0))
print('ABCD: ' + str(mean1))


print('Standard Deviation:')
var_abc  = 0.0
var_abcd = 0.0

for i in range(nRuns):
	var_abc  += (final_abc[i]-mean0) ** 2
	var_abcd += (final_abcd[i]-mean1) ** 2

print('ABC: ' + str(math.sqrt(var_abc)))
print('ABCD: ' + str(math.sqrt(var_abcd)))


# Plot everything
fig, ax1 = plt.subplots()
x = range(nIter)

#real value to log
for i in range(nIter):
	evol0[i] = math.log(evol0[i] + 1e-99, 10) #Some times evol0[i] equals to zero or negative
	evol1[i] = math.log(evol1[i] + 1e-99, 10)


l0 = ax1.plot(x, evol0, label='$ABC$')
l1 = ax1.plot(x, evol1, label='$ABC^d$')
ax1.set_xlabel('Iteration', {'fontsize':14})
ax1.set_ylabel('log(Minimum)'  , {'fontsize':14})

ax2 = ax1.twinx()
ax2.tick_params(axis='y', colors='black')

y_low  = min([min(diversity1),min(diversity2)])
y_high = max([max(diversity1),max(diversity2)])

ax2.set_ylim([y_low-0.2,y_high+0.1])
l2 = ax2.plot(x, diversity1, '--', label='$d(ABC)$')
l3 = ax2.plot(x, diversity2, '--', label='$d(ABC^d)$')
ax2.set_ylabel('Diversity', {'fontsize':14,'color':'k'})
lns = l0+l1+l2+l3

labs = [l.get_label() for l in lns]
ax1.legend(lns, labs, loc="upper right")

title = pb1.name
plt.title(title, {'fontsize':18})
plt.show()
