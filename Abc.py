from problem import Problem
import random as r
import math

'''
ABC algorithm adapted from Java version on:
https://abc.erciyes.edu.tr/software.htm
'''

class Abc:
	np = 0           # Population size (NP)
	foodNumber = 0   # Number of solutions, or employed bees (SN)
	limit = 0        # Visits to make an employed bee turn into a scout
	iteration = 0    # Current iteration
	maxCycle = 0     # Maximum iteration
	pb = Problem(Problem.NONE, 0) # Problem to be solved. None by default

	foods    = list() # Current solutions
	f        = list() # Function value of the functions
	fitness  = list() # Fitness of the function values
	trial    = list() # Number of faild attemps to improve a solution
	prob     = list() # Probability of an observer visit solution i
	solution = list() # Auxiliar vector

	globalMin    = 0.0    # Best function value found so far f(X)
	globalParams = list() # Arguments X of the best solution
	
	def __init__(self, in_pb, in_np, in_limit, in_maxCycle):
		#in_pb: problem to be solved
		#in_np: population size (employed+unemployed)
		#in_limit: abandon after limit visitations with no improvement
		#in_maxCycle: maximum number of cycles (iterations)
		self.pb = in_pb
		self.np         = in_np
		self.foodNumber = in_np//2
		self.limit      = in_limit
		self.iteration  = 0
		self.maxCycle   = in_maxCycle

		self.foods = [[0] * (self.pb.D) for i in range(self.foodNumber)]
		self.f     = [None]*(self.foodNumber)
		self.fitness = [None]*(self.foodNumber)
		self.trial = [None]*(self.foodNumber)
		self.prob = [None]*(self.foodNumber)
		self.solution = [None]*(self.pb.D)
		
		self.globalMin = [None]*(self.pb.D)
		self.globalParams = [None]*(self.pb.D)
	
	def initall(self):
		# Init whole population
		for i in range(self.foodNumber):
			self.init(i)
		# Memorize best individual
		self.globalMin = self.f[0]
		for i in range(self.pb.D):
			self.globalParams[i] = self.foods[0][i]

	def init(self, index):
		# Each employed bee is a current solution.
		# The values are a limited to the search space
		LB = self.pb.LB # Lower bound
		UB = self.pb.UB # Upper bound

		for j in range(self.pb.D):
			self.foods[index][j] = r.random()*(UB-LB)+LB
			self.solution[j] = self.foods[index][j]

		self.f[index] = self.pb.calculateFunction(self.solution)
		self.fitness[index] = self.calculateFitness(self.f[index])
		self.trial[index] = 0

	def iterate(self):
		# Algorithm steps
		self.sendEmployedBees()
		self.calculateProbabilities()
		self.sendOnLookerBees()
		self.memorizeBestSource()
		self.sendScoutBees()
		self.iteration = self.iteration + 1
	
	def sendEmployedBees(self):
		# Search equation causes a pertubation to a current solution
		# The perturbation is executed once for all solution on this phase
		for i in range(self.foodNumber):
			self.updateSolution(i) # Perturbation

			# Quality of the solution
			objValSol = self.pb.calculateFunction(self.solution)
			fitnessSol = self.calculateFitness(objValSol)

			# Keep it only if it improves the current solution
			if fitnessSol > self.fitness[i]:
				self.trial[i] = 0
				for j in range(self.pb.D):
					self.foods[i][j] = self.solution[j]
				self.f[i] = objValSol
				self.fitness[i] = fitnessSol
			else:
				# If not, discard and increase trial counter for solution i
				self.trial[i] = self.trial[i]+1

	def calculateProbabilities(self):
		# Probability of a onlooker bee visit solution i. as described in the paper
		maxFit = self.fitness[0]
		for i in range(self.foodNumber):
			if self.fitness[i] > maxFit:
				maxFit = self.fitness[i]
		
		for i in range(self.foodNumber):
			self.prob[i] = (0.9*(self.fitness[i]/maxFit))+0.1

	def sendOnLookerBees(self):
		# Aproximation of an roulette wheel
		# One may note the procedure differs a little from the convencional roulette wheel selection.
		# In fact, the procedure proposed above is not equivalent, but it's approximated. The advantage
		# is the gain on speed. With a conventional roulette, the consumed time would be bigger.
		i = 0
		t = 0
		while t < self.foodNumber:
			if r.random() < self.prob[i]:
				t = t+1
				self.updateSolution(i)

		                objValSol = self.pb.calculateFunction(self.solution)
                		fitnessSol = self.calculateFitness(objValSol)
				
				if fitnessSol > self.fitness[i]:
                                	self.trial[i] = 0
	                                for j in range(self.pb.D):
        	                                self.foods[i][j] = self.solution[j]
                	                self.f[i] = objValSol
                        	        self.fitness[i] = fitnessSol
                        	else:
                                	self.trial[i] = self.trial[i]+1
			i = i+1
			if i == self.foodNumber:
				i = 0

	def memorizeBestSource(self):
		# Well, the name says it all
		for i in range(self.foodNumber):
			if self.f[i] < self.globalMin:
				self.globalMin = self.f[i]
				for j in range(self.pb.D):
					self.globalParams[j] = self.foods[i][j]

	def sendScoutBees(self):
		# Scout bees go to a random location of the space
		maxTrialIndex = 0
		for i in range(self.foodNumber):
			if self.trial[i] > self.trial[maxTrialIndex]:
				maxTrialIndex = i
		if self.trial[maxTrialIndex] >= self.limit:
			self.init(maxTrialIndex)

	def updateSolution(self, i):
		# Pertubation equation
		neighbor = r.randrange(self.foodNumber)
		while self.foodNumber > 1 and neighbor == i:
			neighbor = r.randrange(self.foodNumber)

		for j in range(self.pb.D):
			self.solution[j] = self.foods[i][j]

		param2change = r.randrange(self.pb.D)
		self.solution[param2change] = self.foods[i][param2change]+(self.foods[i][param2change]-self.foods[neighbor][param2change])*(r.random()-0.5)*2;

		# Makes sure no solution is invalid.
		if self.solution[param2change] < self.pb.LB:
			self.solution[param2change] = self.pb.LB
		if self.solution[param2change] > self.pb.UB:
			self.solution[param2change] = self.pb.UB
			

	def calculateFitness(self,fun):
		result = 0
		if fun >= 0:
			result = 1.0/(fun+1)
		else:
			result = 1.0 + abs(fun)
		return result

	def popDiversity(self):
		# A diversity measure for the population. More details in:
		# A Diversity-Guided Particle Swarm Optimizer - the ARPSO
		# Ridget, J. and Vestertrom, J. S. (2002)

		S = self.foodNumber                             # Constant elements in the swarm
		L = abs(self.pb.UB-self.pb.LB) * math.sqrt(2)   # Biggest diagonal
		D = self.pb.D                                   # Dimensionality of the problem

		p = [0.0]*D                                     # Mean point
		for j in range(D):
			for i in range(S):
				p[j] += self.foods[i][j]
			p[j] = p[j] / S
		
		diversity = 0.0
		for i in range(S):
			var = 0.0
			for j in range(D):
				var += (self.foods[i][j] - p[j]) ** 2
			diversity += math.sqrt(var)
		
		return diversity/(S*L)


