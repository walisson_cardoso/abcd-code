from problem import Problem
import random as r
import math
import pdb;

'''
ABC with modified search equation and diversity control
Almost all code is similar to ABC, so the comments there also apply here for most part
'''


class Abcd:
	np = 0
	foodNumber = 0
	iteration = 0
	maxCycle = 0
	pb = Problem(Problem.NONE, 0)
	limit = 0
	initialLimit = 0
	initialDiversity = 0.0 # The population diversity on the first iteration
	isOnLimit = False      # Is the limit parameter on reduction state?

	foods    = list()
	f        = list()
	fitness  = list()
	trial    = list()
	prob     = list()

	globalMin    = 0.0
	globalParams = list()
	
	f = list()

	def __init__(self, in_pb, in_np, in_limit, in_maxCycle):
		#in_pb: problem to be solved
		#in_np: population size (employed+unemployed)
		#in_limit: abandon after limit visitations with no improvement
		#in_maxCycle: maximum number of cycles (iterations)
		self.pb = in_pb
		self.np         = in_np
		self.foodNumber = in_np//2
		self.limit      = in_limit
		self.initialLimit = in_limit
		self.iteration  = 0
		self.maxCycle   = in_maxCycle

		self.foods = [[0] * (self.pb.D) for i in range(self.foodNumber)]
		self.f     = [None]*(self.foodNumber)
		self.fitness = [None]*(self.foodNumber)
		self.trial = [None]*(self.foodNumber)
		self.prob = [None]*(self.foodNumber)
		
		self.globalMin = [None]*(self.pb.D)
		self.globalParams = [None]*(self.pb.D)
	
	def initall(self):
		for i in range(self.foodNumber):
			self.init(i)

		self.globalMin = self.f[0]
		for i in range(self.pb.D):
			self.globalParams[i] = self.foods[0][i]
		self.initialDiversity = self.popDiversity()

	def init(self, index):
		LB = self.pb.LB
		UB = self.pb.UB

		for j in range(self.pb.D):
			self.foods[index][j] = r.random()*(UB-LB)+LB
		solution = self.foods[index][:]

		self.f[index] = self.pb.calculateFunction(solution)
		self.fitness[index] = self.calculateFitness(self.f[index])
		self.trial[index] = 0

	def iterate(self):
		self.sendEmployedBees()
		self.calculateProbabilities()
		self.sendOnLookerBees()
		self.memorizeBestSource()
		self.sendScoutBees()
		self.updateLimit()
		self.iteration = self.iteration + 1
	
	# Note that almost all functions have shrinked. The ABC version
	# repeats it self a lot of times with the bouding of solutions and
	# checking if some improvement was made. I solved this by simply
	# moving these steps to the updateSolution function.
	def sendEmployedBees(self):
		for i in range(self.foodNumber):
			improved = self.updateSolution(i, "employed")

	def calculateProbabilities(self):
		maxFit = self.fitness[0]
		for i in range(self.foodNumber):
			if self.fitness[i] > maxFit:
				maxFit = self.fitness[i]
		
		for i in range(self.foodNumber):
			self.prob[i] = (0.9*(self.fitness[i]/maxFit))+0.1

	def sendOnLookerBees(self):
		i = 0
		t = 0
		while t < self.foodNumber:
			if r.random() < self.prob[i]:
				t = t+1
				improved = self.updateSolution(i, "onLooker")
		                
			i = i+1
			if i == self.foodNumber:
				i = 0

	def memorizeBestSource(self):
		for i in range(self.foodNumber):
			if self.f[i] < self.globalMin:
				self.globalMin = self.f[i]
				for j in range(self.pb.D):
					self.globalParams[j] = self.foods[i][j]

	def sendScoutBees(self):
		maxTrialIndex = 0		
		for i in range(self.foodNumber):
			if self.trial[i] > self.trial[maxTrialIndex]:

				maxTrialIndex = i
		if self.trial[maxTrialIndex] >= self.limit:
			self.init(maxTrialIndex)

	def updateSolution(self, i, beeType):
		neighbor = r.randrange(self.foodNumber)
		while self.foodNumber > 1 and neighbor == i:
			neighbor = r.randrange(self.foodNumber)

		solution = self.foods[i][:]

		param2change = r.randrange(self.pb.D)
		diff = (self.foods[i][param2change]-self.foods[neighbor][param2change])
		# Proposed modification
		# -diff/2 is equivalent to move the alpha phi variable to [-1.5,+0.5]
		solution[param2change] = self.foods[i][param2change]+diff*(r.random()-0.5)*2 - diff/2

		if solution[param2change] < self.pb.LB:
			solution[param2change] = self.pb.LB
		if solution[param2change] > self.pb.UB:
			solution[param2change] = self.pb.UB

		objValSol = self.pb.calculateFunction(solution)
		fitnessSol = self.calculateFitness(objValSol)
	
		if fitnessSol > self.fitness[i]:
                	self.trial[i] = 0
                        for j in range(self.pb.D):
                                self.foods[i][j] = solution[j]
	                self.f[i] = objValSol
        	        self.fitness[i] = fitnessSol
			return True
        	else:
                	self.trial[i] = self.trial[i]+1
			return False
			

	def calculateFitness(self,fun):
		result = 0
		if fun >= 0:
			result = 1.0/(fun+1)
		else:
			result = 1.0 + abs(fun)
		return result

	def updateLimit(self):
		# It diversity decreases to one tenth, cut limit by half
		if self.popDiversity() < self.initialDiversity/10:
			self.isOnLimit = True
			self.limit = self.initialLimit/2
		else:
			self.isOnLimit = False
			self.limit = self.initialLimit

	def popDiversity(self):
		# A diversity measure for the population. More details in:
		# A Diversity-Guided Particle Swarm Optimizer - the ARPSO
		# Ridget, J. and Vestertrom, J. S. (2002)

		S = self.foodNumber                             # Constant elements in the swarm
		L = abs(self.pb.UB-self.pb.LB) * math.sqrt(2)   # Biggest diagonal
		D = self.pb.D                                   # Dimensionality of the problem

		pm = [0.0]*(D)                                  # Mean point

		for j in range(D):
			for i in range(S):
				pm[j] += self.foods[i][j]
			pm[j] = pm[j] / S
		
		diversity = 0.0
		for i in range(S):
			var = 0.0
			for j in range(D):
				var += (self.foods[i][j] - pm[j]) ** 2
			diversity += math.sqrt(var)
		
		return diversity/(S*L)


