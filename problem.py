import math
import numpy as np

class Problem:
	calls = 0     # The number of times a function was called for a instance of this class
	function = 0  # Function index
	name = "None" # Function name
	D = 10        # Dimensionality (default = 10)
	LB = -5.12    # lowest value possible  (default = Rastrigin)
	UB =  5.12    # highest value possible (default = Rastrigin)
	
	# Lists of lower and upper bounds and functions names in its respective order
	listLB = [0.0, -5.12, -32.768, -600, -512, -5,       0, -500, -100]
	listUB = [0.0,  5.12,  32.768,  600,  512, 10, math.pi,  500,  100]
	names  = ["None", "Rastrigin", "Ackley", "Griewank", "Egg Holder", "Rosenbrock", "Michalewicz", "Schwefel", "Sum Of Square"]
	
	# Function indexes
	NONE = 0
	RASTRIGIN = 1
	ACKLEY = 2
	GRIEWANK = 3
	EGGHOLDER = 4
	ROSENBROCK = 5
	MICHALEWICZ = 6
	SCHWEFEL = 7
	SUMOFSQUARE = 8

	# Initializes instance with a specified function given by its index
	def __init__(self, function, D):
		self.calls    = 0
		self.function = function
		self.D        = D
		
		if function >= 0 and function <= 8:
			self.LB = self.listLB[function]
			self.UB = self.listUB[function]
			self.name = self.names[function]
			self.D  = D
			if function == self.NONE:
				self.D = 0
			if function == self.EGGHOLDER:
				self.D = 2
		else:
			print("Invalid function ID")
	
	def calculateFunction(self, sol):
		self.calls = self.calls + 1
		
		if   self.function == self.NONE:
			return 0.0
		elif self.function == self.RASTRIGIN:
			return self.rastrigin(sol)
		elif self.function == self.ACKLEY:
			return self.ackley(sol)
		elif self.function == self.GRIEWANK:
			return self.griewank(sol)
		elif self.function == self.EGGHOLDER:
			return self.eggHolder(sol)
		elif self.function == self.ROSENBROCK:
			return self.rosenbrock(sol)
		elif self.function == self.MICHALEWICZ:
			return self.michalewicz(sol)
		elif self.function == self.SCHWEFEL:
			return self.schwefel(sol)
		elif self.function == self.SUMOFSQUARE:
			return self.sumOfSquare(sol)
		else:
			print("You entered a invalid function ID")


	def rastrigin(self, sol):
		top = 0.0
		for i in range(self.D):
			xi  = sol[i]
			top = top + ((xi**2) - 10*np.cos(2*math.pi*xi))
			
		return 10*self.D + top

	def ackley(self, sol):
		a = 20
		b = 0.2
		c = 2*math.pi
		d = self.D
		
		sum1 = 0.0
		sum2 = 0.0

		for i in range(d):
			xi = sol[i]
			sum1 = sum1 + xi**2
			sum2 = sum2 + math.cos(c*xi)

		term1 = -a*math.exp(-b*math.sqrt(sum1/d))
		term2 = -math.exp(sum2/d)

		return term1 + term2 + a + math.exp(1)

	def griewank(self, sol):
		top  = 0.0
		prod = 1.0

		for i in range(self.D):
			xi = sol[i]
			top = top + (xi**2)/4000.0
			prod = prod * math.cos(xi/math.sqrt(i+1))
		
		return top - prod + 1

	def eggHolder(self, sol):
		x1 = sol[0]
		x2 = sol[1]

		term1 = -(x2+47) * math.sin(math.sqrt(abs(x2+x1/2.0+47)))
		term2 = -x1      * math.sin(math.sqrt(abs(x1-(x2+47))))

		return term1 + term2

	def rosenbrock(self, sol):
		top = 0.0
		for i in range(self.D-1):
			xi = sol[i]
			xnext = sol[i+1]
			n = 100*((xnext-xi**2)**2) + (xi-1)**2
			top = top + n
			
		return top

	def michalewicz(self, sol):
		m = 10.0
		top = 0.0

		for i in range(self.D):
			xi = sol[i]
			n = math.sin(xi) * ((math.sin((i+1)*(xi**2)/math.pi)) ** (2*m))
			top = top + n

		return -top

	def schwefel(self, sol):
		top = 0.0
		for i in range(self.D):
			xi = sol[i]
			top = top + xi*math.sin(math.sqrt(abs(xi)))

		return 418.98288727243379*self.D - top

	def sumOfSquare(self, sol):
		top = 0.0
		for i in range(self.D):
			xi = sol[i]
			top = top + (i+1)*(xi**2)
		
		return top
